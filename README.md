# Testing ISO-12914:2012 for ICP-OES quantification of aqua-regia-extractable metals
## Description

Internally validate the ISO 12914:2012(E)[[1]](#1) for our laboratory procedures
and equipment using the ERM-CC144[[2]](#2) certified sewage sludge
[(pdf)](./documentation/ERM-CC144--Certified-sewage-sludge-ISO12914.pdf) and a
set of test samples from the full-scale _microtransitions_ study.

## Project status

- **2021-10-29**.  The project is under development.  First round of samples
  have been digested and will soon be measured with ICP to evaluate the
  digestion efficacy.

## Reflection
## Authors

- Marco Prevedello <m.prevedello1@nuigalway.ie> [@Preve92](https://twitter.com/Preve92)

## License
## Acknowledgments
## Bibliography

<a id="1">[1]</a> ISO 12914:2012(E), Soil quality — Microwave-assisted
extraction of the aqua regia soluble fraction for the determination of
elements. 2012-02-01. International Organization for Standardization, Geneva,
Switzerland. https://www.iso.org/standard/52171.html

<a id="2">[2]</a> European Commission. Joint Research Centre. Institute for
Reference Materials and Measurements. 2016. The Certification of the Mass
Fraction of the Total Content and the Aqua Regia Extractable Content (ISO 12914
and ISO 11466) of As, Cd, Co, Cr, Cu, Fe, Hg, Mn, Ni, Pb and Zn in Sewage
Sludge, ERM®- CC144: Certification Report. LU: Publications Office.
https://data.europa.eu/doi/10.2787/264881.
